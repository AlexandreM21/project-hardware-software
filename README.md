# Ydays Hardware-Software

Très intéressé par ce Ydays, je vais pouvoir en apprendre davantage sur l'intégralité de mon ordinateur qui est actuellement mon outil de travail.

Pouvoir régler n'importe quelle panne informatique dans le futur sera mon objectif à la fin de ce Ydays.

## Compte Rendu 26 Octobre 2022

Début silencieux, beaucoup de timides, mais sérieux sur les présences. Maxence et Eve ont su combler ce vide avec leur expérience.
On s'est senti à l'aise après la petite introduction et le petit brunch du matin.

Diapo très instructive, j'ai pu apprendre l'assemblage d'un pc suivant un ordre précis, la vitesse de la RAM,
savoir que chaque ordinateur (portable) possède son propre câble de référence et qu'il faut regarder le nombre de mAh quand on en prend un autre
qui pourrait entrainer la surchauffe de l'alimentation.

J'ai pu voir l'intérieur d'un HDD et comprendre son fonctionnement,
comprendre les différents bus de communications qu'utilisent chaque composant. Peut être passé un peu trop vite là-dessus,
j'avais oublié certains points quand on est passé au quizz les acronymes ne m'ont pas aidés.

Faire attention à ne pas apporter d'aimants près du HDD ou on risquerait de faire dysfonctionner la tête de lecture.

/!\ Vérifier la garantie sinon adios le pc /!\

```bash
Un doigt en moins durant cette séance. Je l`ai mis dans du riz : aucun effet.
```

## Compte Rendu 16 Novembre 2022

Début de journée sur une présentation des composants sur une carte mère.
Nous avons vu comment assembler les composants de l'ordinateur hors boitier.
Important : le ventirad se raccorde au "CPU_FAN". Ne pas oublier la pâte thermique entre le ventirad et le processeur.

Les ventilateurs se raccordent au SYSFANX (X représente un chiffre). Il existe 3 système de fixation possible : Rack classique, tiroirs amovibles, fixation avec ou sans vis.
Ne pas se tromper dans le placement du ventilateur.

Il doit toujours être positionné contre une paroi possédant une grille d'aération. Le panneau frontal se situe au niveau du connecteur "FRONT_PANEL" ou "F_PANEL".
Il est important de bien les brancher, car il respecte un ordre précis.
Quant à la carte graphique, il faut qu'elle soit connectée à "PCI" et l'alimentation "PCI-E".

Le plus dur à faire reste sans contest le câble management...

## Compte Rendu 30 Novembre 2022

Début de journée tranquille. Matthias nous a raconté le background des sockets concernant les processeurs AMD et INTEL.
Intel est mieux que AMD en terme de performance pure mais est très unique et reste surtout incompatible avec les autres versions afin d'assurer leur chiffre d'affaires.
Intel offre un potentiel plus élevé que les autres. Par exemple, Intel a amélioré la vitesse de calcul en plus des performances graphiques, qui sont la référence pour les jeux vidéo.

Contrairement à AMD qui vise à s'adapter à tout type de version et surtout dans le bas ainsi que le milieu de gamme, il se différencie de son concurrent en possédant un meilleur overcloacking.

Matthias nous a expliqué la structure au niveau des processeur avec comme exemple 10900K : 10 correspond à la version de l'OS, 9 à la version du core et les 00 pour faire joli (ils aiment mettrent des zéro), et le K permet d'overclock le bios.

On a ensuite vu des différentes carte mère existante au niveau de leur format : EATX, ATX, micro-ATX et mini-ITX.
Bien vérifier à prendre la carte mère avant d'acheter le boitier afin de prendre un boitier qui puisse accueillir ce dernier.

Pour finir, nous avons remonté un ordinateur fixe pour se préparer à la prochaine séance évaluée sans aide.
Puis nous avons écouté la conférence de Marine sur la première souris crée dans le monde par Douglas.

## Compte Rendu 14 Décembre 2022

Début de journée sur la présentation de l'examen. 10h00-11h00 : Démontage des ordinateurs.
Maxence nous a fait une petite introduction sur les différents types de  ̶p̶r̶o̶f̶s̶ vis (teknik celle là).
Trois présentations très instructives se sont déroulées notamment sur la présentation de facebook et sur les différents types de ventilateurs
ainsi que comment choisir le bon. 

14h00, début de l'examen: prendre son temps afin de ne pas faire d'erreurs lors du montage de l'ordinateur. 
16h00: fin du montage, vérification du câble management, vérification que l'ordinateur s'allume bien, fermeture des deux panels.
Fin du premier examen. Repos soldat.

## Compte Rendu 11 Janvier 2023

9h30 : rentrée tranquille, petit temps d'attente pendant que les absents effectuaient leur examen. Pendant ce temps, deux élèves ont présenté leur point culture : le phone phreaking ainsi que le record de vitesse de téléchargement.
J'en ai profité pour faire un peu de R&D au niveau d'UX et UI via un site internet très cool appelé Uxcel. 
L'après-midi, petit point sur le bios, à savoir comment vérifier sa version et l'upgrade. 
Regarder la virtualisation au niveau du bios afin de pouvoir lancer des machines virtuelles.
Puis petit team-building avec des étudiants afin de créer une ambiance calme et pouvoir sympathiser avec l'espèce humaine afin de mieux coordonner la future invasion interplanétaire. 

## Suite prochaine séance

## Merci à Maxence GILLES et Eve ANDRÉ-BRENNER de nous faire découvrir ce vaste monde \o/
[Le repère caché des laborantins](https://discord.com/invite/JvWqV88AnG)